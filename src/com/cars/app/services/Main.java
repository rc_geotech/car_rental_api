package com.cars.app.services;

import com.cars.app.domain.CarRent;
import java.time.Instant;
import java.util.Date;

public class Main {

  public static void main(String[] args) {
    CarRent carRent =
        CarRent.of(
            "carRent-1",
            "user1",
            "car1",
            Date.from(Instant.now().minusSeconds(20_000)),
            Date.from(Instant.now()),
            "license1",
            200);

    double bill = carRent.calculateBill(false, false, 0, 25);
    System.out.println(bill);
  }
}
