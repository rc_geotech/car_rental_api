package com.cars.app.domain;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class CarRent {
  private String id;
  private String userId;
  private String car_id;
  private String payment_mode;
  private boolean isDoorstepDrop;
  private double amount;
  private Date startDateAndTime;
  private Date endDateAndTime;
  private boolean hasDamaged;
  private double damagingCharges;
  private double kilometerTravelled;
  private String licenseNumber;
  private String damageDetails;

  private CarRent(
      String id,
      String userId,
      String car_id,
      Date startDateAndTime,
      Date endDateAndTime,
      String licenseNumber,
      double kilometerTravelled) {
    this.id = id;
    this.userId = userId;
    this.car_id = car_id;
    this.startDateAndTime = startDateAndTime;
    this.endDateAndTime = endDateAndTime;
    this.licenseNumber = licenseNumber;
    this.kilometerTravelled = kilometerTravelled;
  }

  public static CarRent of(
      String id,
      String userId,
      String car_id,
      Date startDateAndTime,
      Date endDateAndTime,
      String licenseNumber,
      double kilometerTravelled) {
    return new CarRent(
        id, userId, car_id, startDateAndTime, endDateAndTime, licenseNumber, kilometerTravelled);
  }

  public static double BOOKING_ADVANCE_CHARGES = 2000;
  public static double DOORSTEP_DROP_CHARGES = 200;
  public static double RATE_PER_KILOMETER = 20;
  public static double RATE_PER_HOUR = 80;

  public double calculateBill(
      boolean isDoorstepDrop,
      boolean hasDamaged,
      double damagingCharges,
      double kilometerTravelled,
      Date startDateAndTime,
      Date endDateAndTime) {
    this.amount = BOOKING_ADVANCE_CHARGES;
    amount = isDoorstepDrop ? amount + DOORSTEP_DROP_CHARGES : amount;
    amount = hasDamaged ? amount + damagingCharges : amount;
    double rentInRatePerHour = calculateBillByRatePerHour(amount, startDateAndTime, endDateAndTime);
    double rentInKilometerPerHour = calculateBillByKilometerPerHour(amount, kilometerTravelled);
    return rentInRatePerHour > rentInKilometerPerHour ? rentInRatePerHour : rentInKilometerPerHour;
  }

  public double calculateBill(
      boolean isDoorstepDrop,
      boolean hasDamaged,
      double damagingCharges,
      double kilometerTravelled) {
    this.amount = BOOKING_ADVANCE_CHARGES;
    amount = isDoorstepDrop ? amount + DOORSTEP_DROP_CHARGES : amount;
    amount = hasDamaged ? amount + damagingCharges : amount;
    double rentInRatePerHour = calculateBillByRatePerHour(amount, this.startDateAndTime, this.endDateAndTime);
    double rentInKilometerPerHour = calculateBillByKilometerPerHour(amount, kilometerTravelled);
    return rentInRatePerHour > rentInKilometerPerHour ? rentInRatePerHour : rentInKilometerPerHour;
  }

  private double calculateBillByRatePerHour(
      double amount, Date startDateAndTime, Date endDateAndTime) {
    long rentTimeInMilliSeconds = endDateAndTime.getTime() - startDateAndTime.getTime();
    TimeUnit timeUnit = TimeUnit.HOURS;
    long rentTimeInHours = timeUnit.convert(rentTimeInMilliSeconds, TimeUnit.MILLISECONDS);

    System.out.println(rentTimeInHours);

    double rentCharges = rentTimeInHours * RATE_PER_HOUR;
    return amount + rentCharges;
  }

  private double calculateBillByKilometerPerHour(double amount, double kilometerTravelled) {
    return amount + kilometerTravelled * RATE_PER_KILOMETER;
  }
}
