package com.cars.app.domain;

public class Car {
  private String id;
  private String name;
  private String number_plate_id;
  private String category;
  private double kilometerTravelled;
  private boolean isAvailable;

  public Car(String id, String name, String number_plate_id, String category) {
    this.id = id;
    this.name = name;
    this.number_plate_id = number_plate_id;
    this.category = category;
    this.isAvailable = true;
    this.kilometerTravelled = 0;
  }
}
